#include "cyton_velocity.h"
#include <ros/ros.h>
#include <thread>

int main(int argc, char** argv) {
    ros::init(argc, argv, "robotics_cyton", ros::init_options::AnonymousName);
    ros::NodeHandle nh;


    ROS_INFO("System Activated");
    CytonVelocity c1(nh);
    c1.run();
    ros::Rate rate(10);

    ros::waitForShutdown();
    c1.stop();





    return 0;

}
